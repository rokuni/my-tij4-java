package com.rokuni.exercises.access_control.another_package;

public class Dog {
    String name;
    int age;

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void info() {
        System.out.println("my name is " + name);
        System.out.println("im " + age + " years old");
    }
}
