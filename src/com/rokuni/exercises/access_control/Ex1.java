package com.rokuni.exercises.access_control;

import com.rokuni.exercises.access_control.another_package.Dog;

public class Ex1 {

    public static void main(String[] args) {
        Dog dog = new Dog("Jin", 3);
        dog.info();
    }

}
